import java.util.StringTokenizer;

public class MorseCodeDecoder {
    public static String decode(String morseCode) {
        // your brilliant code here, remember that you can access the preloaded Morse code table through MorseCode.get(code)
        StringTokenizer tokenizer = new StringTokenizer(morseCode, " \t\n\r\f", true);
        StringBuffer result = new StringBuffer();
        int nbWhite = 0;
        boolean firstNonWhiteCharEncountered = false;
        while (tokenizer.hasMoreElements()) {
          String token = tokenizer.nextToken();
          if (Character.isWhitespace(token.charAt(0))) {
              if (nbWhite == 1 && firstNonWhiteCharEncountered) {
                  result.append(' ');
              }
              nbWhite++;
          } else {
              nbWhite = 0;
              firstNonWhiteCharEncountered = true;
              result.append(MorseCode.get(token));
          }
        }
        if (nbWhite > 1 && firstNonWhiteCharEncountered) {
            result.deleteCharAt(result.length() - 1);
        }
        return result.toString();
    }
}