In this Kata you will rotate a binary tree. You need to implement two methods, which rotate a binary tree. One is to rotate it to the left, another one will rotate a tree to the right.

If rotation is impossible, return the tree unchanged.

###Node structure

    Node {
        Node left
        Node right
        int value
    }

What is a binary tree?

A binary tree is a tree graph, in which each element can't have more than 2 children.

What does rotate mean?

What does it mean to rotate a binary tree in this case? The rotation is changing a root of the tree to be next or previous child of the current root. For example:

        9
       / \
      7   11
     / \
    5   8

In this case the root is 9, its left child is 7 and right child is 11.

If we rotate it to the right, we'll get a tree like that:

        7
       / \
      5   9
     / \
    8   11

We switch a left child of the old root (9) to 7th right child and made 7 to be the new root.

If we rotate it to the left, we'll get another tree:

          11
         /
        9
       /
      7
     / \
    5   8

We switch a right child of an old root 9 to be null, set it as a left child to 11 and make 11 be the new root.

Why to rotate it?

https://twitter.com/mxcl/status/608682016205344768

Good luck!