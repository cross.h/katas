package com.codewars.solution;

public class Spreader {

    public Node rotateRight(Node root) {
        if (root.left == null) return root;

        Node oldRoot = root;
        root = root.left;

        oldRoot.setLeft(root.right);
        root.setRight(oldRoot);

        return root;
    }

    public Node rotateLeft(Node root) {
        if (root.right == null) return root;

        Node oldRoot = root;
        root = root.right;

        oldRoot.setRight(root.left);
        root.setLeft(oldRoot);

        return root;
    }
}