package com.codewars.solution;

public class Node {
    public Node left;
    public Node right;
    private int value;

    public Node(int value) {
        setValue(value);
    }

    public Node getLeft() {
        return left;
    }

    public Node setLeft(Node left) {
        this.left = left;
        return this;
    }

    public Node getRight() {
        return right;
    }

    public Node setRight(Node right) {
        this.right = right;
        return this;
    }

    public int getValue() {
        return value;
    }

    public void setValue(int value) {
        this.value = value;
    }

    @Override
    public String toString() {
        return Integer.toString(getValue());
    }

    @Override
    public boolean equals(Object other) {
        if (! (other instanceof Node))
            return false;
        Node node = (Node) other;
        return node.getValue() == getValue()
                && (getLeft() == null ? node.getLeft() == null : getLeft().equals(node.getLeft()))
                && (getRight() == null ? node.getRight() == null : getRight().equals(node.getRight()));
    }
}
