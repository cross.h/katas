package com.codewars.solution;

import org.junit.*;
import static org.junit.Assert.assertThat;
import static org.hamcrest.core.Is.is;

public class SpreaderTest {

    private Spreader spreader;
    private Node initialNode = new Node(5)
            .setLeft(new Node(2))
            .setRight(new Node(8)
                    .setLeft(new Node(7))
                    .setRight(new Node(12)));

    @Before
    public void setUp() throws Exception {
        spreader = new Spreader();
    }

    @Test
    public void rotateRight_shouldReturnRotatedNode() throws Exception {
        Node resultNode = new Node(2)
                .setRight(new Node(5)
                        .setRight(new Node(8)
                                .setLeft(new Node(7))
                                .setRight(new Node(12))));
        assertThat(spreader.rotateRight(initialNode), is(resultNode));

    }

    @Test
    public void rotateLeft_shouldReturnRotatedNode() throws Exception {
        Node resultNode = new Node(8)
                .setLeft(new Node(5)
                        .setLeft(new Node(2))
                        .setRight(new Node(7)))
                .setRight(new Node(12));
        assertThat(spreader.rotateLeft(initialNode), is(resultNode));
    }
}