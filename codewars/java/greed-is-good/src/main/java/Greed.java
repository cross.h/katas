import java.util.Arrays;

public class Greed {
    public static int greedy(int[] dice) {
        // first column : dice value
        // second column : reward for triplet
        // third column : reward for solo or over triplet
        // fourth column : result for each dice value
        int[][] scoreTable = new int[][] {
                {1, 1000, 100, 0},
                {2, 200, 0, 0},
                {3, 300, 0, 0},
                {4, 400, 0, 0},
                {5, 500, 50, 0},
                {6, 600, 0, 0}
        };
        Arrays.stream(scoreTable)
                .parallel()
                .forEach(
                        i -> {
                            i[3] = (int) Arrays.stream(dice)
                                    .filter(j -> j == i[0])
                                    .count();
                            i[3] = i[3] < 3 ? i[3] * i[2] : i[1] + Math.max(0, i[3] - 3) * i[2];
                        }
                );
        return Arrays.stream(scoreTable).map(t -> t[3]).reduce(Integer::sum).orElse(0);
    }
}
