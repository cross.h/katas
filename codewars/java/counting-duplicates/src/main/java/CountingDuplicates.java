import java.util.Locale;

public class CountingDuplicates {
    public static int duplicateCount(String text) {
        final String lcText = text.toLowerCase(Locale.ROOT);
        return (int) lcText.chars()
                .mapToObj(c -> (char) c)
                .distinct()
                .mapToLong(c -> lcText.chars()
                            .filter(ch -> ch == c)
                            .count())
                .filter(i -> i > 1)
                .count();
    }
}