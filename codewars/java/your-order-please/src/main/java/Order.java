import java.util.Arrays;

public class Order {
    public static String order(String words) {
        if (words.trim().length() == 0)
            return "";
        return Arrays.asList(words.split("\\s")).stream()
                .map(s -> Arrays.asList(new Object[] { s.chars()
                        .mapToObj(c -> (char) c)
                        .filter(c -> Character.isDigit(c))
                        .mapToInt(c -> Integer.parseInt("" + c))
                        .findFirst().getAsInt(), s}))
                .sorted((a, b) -> ((Integer)a.get(0)).compareTo((Integer)b.get(0)))
                .map(l -> (String)l.get(1))
                .reduce((a, b) -> a + " " + b)
                .get();
    }
}