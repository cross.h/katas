import java.util.Arrays;
import java.util.List;

public class Vowels {
    private static final List<Character> VOWELS = Arrays.asList(new Character[] {
            'a', 'e', 'i', 'o', 'u'
    });

    public static int getCount(String str) {
        return (int) str.chars()
                .mapToObj(c -> (char) c)
                .filter(c -> VOWELS.contains(c))
                .count();
    }
}