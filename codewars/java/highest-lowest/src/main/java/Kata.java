import java.util.StringTokenizer;
import java.util.stream.Stream;

public class Kata {
    public static String highAndLow(String numbers) {
        return "" +
                Stream.of(numbers.split(" "))
                        .mapToInt(Integer::parseInt)
                        .max()
                        .getAsInt()
                + " " +
                Stream.of(numbers.split(" "))
                        .mapToInt(Integer::parseInt)
                        .min()
                        .getAsInt();
    }
}
