import java.util.Collections;

class Diamond {
    public static String print(int n) {
        if (n < 0 || n%2 == 0) {
            return null;
        }

        StringBuilder result = new StringBuilder();

        for (int i = 0; i < n; i++) {
            result.append('*');
        }
        result.append('\n');

        StringBuilder current = new StringBuilder();
        StringBuilder spacer = new StringBuilder();
        for (int i = n - 2; i > 0; i = i - 2) {
            spacer.setLength(0);
            for (int j = 0; j < (n - i)/2; j++) {
                spacer.append(' ');
            }

            current.setLength(0);
            current.append(spacer);
            for (int j = 0; j < i; j++) {
                current.append('*');
            }
            current.append('\n');

            result.insert(0, current);
            result.append(current);
        }

        return result.toString();
    }
}